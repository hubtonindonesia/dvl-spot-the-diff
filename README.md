## Mod Usage

`gameurl.domain?gameId={GAME_ID_FROM_BACKEND}&token={AUTH_TOKEN}`

## Modifications
- Ganti waktu jadi 60s - via `edit.html`
- Hanya ada satu level - via `game.js` config
- Perbedaan yang dicari ada 10 - via `edit.html`
- Send score ke external URL (API) - `saveGame` function
- Coba ganti asset

### Modded Files

VSCode: `ctrl`+`shift`+`f`, enter "@MOD" keyword.

- `game.js`
- `canvas.js`
- `loader.js`
- `puzzles.js`
