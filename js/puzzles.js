////////////////////////////////////////////////////////////
// BOARDS
////////////////////////////////////////////////////////////

var puzzles_arr = [
  {
    thumbnail:'assets/puzzles/dvl_1.png',
    image:'assets/puzzles/dvl_1.png',
    hidden:[{x:276, y:451, ratio:20},{x:279, y:354, ratio:20},{x:457, y:592, ratio:20},{x:461, y:255, ratio:30},{x:379, y:438, ratio:20},{x:322, y:586, ratio:30},],
    timer:60000,
  },

  {
    thumbnail:'assets/puzzles/dvl_2.png',
    image:'assets/puzzles/dvl_2.png',
    hidden:[{x:344, y:225, ratio:40},{x:178, y:459, ratio:30},{x:391, y:520, ratio:30},{x:359, y:615, ratio:20},{x:536, y:484, ratio:30},{x:569, y:599, ratio:20},],
    timer:60000,
  },

  {
    thumbnail:'assets/puzzles/dvl_3.png',
    image:'assets/puzzles/dvl_3.png',
    hidden:[{x:451, y:310, ratio:20},{x:490, y:388, ratio:20},{x:537, y:619, ratio:30},{x:493, y:457, ratio:30},{x:281, y:480, ratio:20},{x:170, y:401, ratio:30},],
    timer:60000,
  },

  {
    thumbnail:'assets/puzzles/dvl_4.png',
    image:'assets/puzzles/dvl_4.png',
    hidden:[{x:504, y:298, ratio:20},{x:439, y:477, ratio:25},{x:468, y:436, ratio:25},{x:547, y:494, ratio:30},{x:257, y:566, ratio:20},{x:300, y:309, ratio:20},],
    timer:60000,
  },

  {
    thumbnail:'assets/puzzles/dvl_5.png',
    image:'assets/puzzles/dvl_5.png',
    hidden:[{x:541, y:392, ratio:50},{x:418, y:410, ratio:20},{x:314, y:326, ratio:25},{x:202, y:394, ratio:20},{x:353, y:363, ratio:20},{x:309, y:424, ratio:20},],
    timer:60000,
  },

  ];